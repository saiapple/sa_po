package main

import (
	"database/sql"
	"html/template"
	"math/rand"
	"net/http"
	"strconv"

	"golang.org/x/crypto/bcrypt"
)

// Load login page.
func loadLoginPage(w http.ResponseWriter, r *http.Request) {
	isLoggined(w, r)

	w.Header().Set("Content-Type", "text/html")

	t, err := template.ParseFiles("./templates/login.html")

	if err != nil {
		panic(err)
	}

	err = t.Execute(w, nil)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// Validate login process and set session if email and password are correct.
func processLogin(w http.ResponseWriter, r *http.Request) {
	user := User{}

	loginEmail := r.FormValue("email")
	loginPassword := r.FormValue("password")

	err := db.QueryRow("SELECT id, email, password FROM user WHERE email=?", loginEmail).Scan(
		&user.id,
		&user.email,
		&user.password,
	)

	if err != nil {
		if err == sql.ErrNoRows {
			apiResponseTemplate(w, "false", "Incorrect password or email!", http.StatusUnprocessableEntity)
			return
		}

		apiResponseTemplate(w, "false", "Failed validate user info1!", http.StatusInternalServerError)
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.password.String), []byte(loginPassword))
	if err != nil {
		apiResponseTemplate(w, "false", "Incorrect password or email!", http.StatusUnprocessableEntity)
		return
	}

	// Generate unique session key
	keySuccess, sessionKey, err := generateUniqueSessionKey(user)

	if !keySuccess || err != nil {
		apiResponseTemplate(w, "false", "Failed validate user info2!", http.StatusInternalServerError)
		return
	}

	setSession(w, r, "userSession", sessionKey)

	apiResponseTemplate(w, "true", "User Login success!", http.StatusOK)
}

// Handle login with google process
func processLoginGoogle(w http.ResponseWriter, r *http.Request) {
	user := User{}

	gmail := r.FormValue("gmail")

	err := db.QueryRow("SELECT id FROM user WHERE gmail=?", gmail).Scan(
		&user.id,
	)

	if err != nil {
		if err == sql.ErrNoRows {
			apiResponseTemplate(w, "false", "Account didn't exists!", http.StatusUnprocessableEntity)
			return
		}

		apiResponseTemplate(w, "false", "Failed validate user info!", http.StatusInternalServerError)
		return
	}

	keySuccess, sessionKey, err := generateUniqueSessionKey(user)

	if !keySuccess || err != nil {
		apiResponseTemplate(w, "false", "Failed validate user info!", http.StatusInternalServerError)
		return
	}

	setSession(w, r, "userSession", sessionKey)

	apiResponseTemplate(w, "true", "User Login success!", http.StatusOK)
}

// Function to generate unique session for each login
func generateUniqueSessionKey(user User) (bool, string, error) {
	isUnique := false

	validKey := ""

	for !isUnique {
		randInt := strconv.Itoa(rand.Intn(100))
		hashedKey := strconv.Itoa(user.id) + randInt
		sessionKey, err := bcrypt.GenerateFromPassword([]byte(hashedKey), bcrypt.DefaultCost)

		if err != nil {
			return false, "", err
		}

		var checkKey string

		err = db.QueryRow("SELECT session_token FROM user_session WHERE session_token=?", sessionKey).Scan(&checkKey)

		if err != nil && err != sql.ErrNoRows {
			return false, "", err
		}

		if checkKey == string(sessionKey) {
			continue
		}

		_, err = db.Exec("INSERT INTO user_session(user_id, session_token) VALUES (?, ?)", user.id, sessionKey)

		if err != nil {
			return false, "", err
		}

		isUnique = true
		validKey = string(sessionKey)
	}

	return true, validKey, nil
}
