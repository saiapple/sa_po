-- Create schema
CREATE TABLE `user` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `phone` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `gmail` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `profile_complete` int(1) unsigned NOT NULL DEFAULT 0,
    `password` varchar(191) COLLATE utf8mb4_unicode_ci NULL,
    PRIMARY KEY (`id`)
)

CREATE TABLE `user_session` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `user_id` int(10) unsigned NOT NULL,
    `session_token` text COLLATE utf8mb4_unicode_ci NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`user_id`) REFERENCES `user`(`id`)
)

CREATE TABLE `user_reset_key` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `user_id` int(10) unsigned NOT NULL,
    `reset_key` text COLLATE utf8mb4_unicode_ci NOT NULL,
    `is_valid` int(1) unsigned NOT NULL DEFAULT 1,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`user_id`) REFERENCES `user`(`id`)
)