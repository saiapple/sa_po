package main

import (
	"database/sql"
	"flag"
	"fmt"
	"net/http"
	"os"

	"github.com/gorilla/sessions"
	"github.com/joho/godotenv"

	_ "github.com/go-sql-driver/mysql"
)

var db *sql.DB
var err error
var store *sessions.CookieStore

func init() {
	// Load envoriment variable
	err := godotenv.Load()

	if err != nil {
		panic(err.Error())
	}

	store = sessions.NewCookieStore([]byte(os.Getenv("SESSION_KEY")))
	store.Options = &sessions.Options{
		Path:     "/",
		MaxAge:   3600 * 1, // 1 hour
		HttpOnly: true,
	}
}

func main() {
	// Connect db.
	db, err = sql.Open("mysql", os.Getenv("DB_USERNAME")+":"+os.Getenv("DB_PASSWORD")+"@tcp("+os.Getenv("DB_HOST")+":"+os.Getenv("DB_PORT")+")/"+os.Getenv("DB_NAME")+"")
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	err = db.Ping()
	if err != nil {
		panic(err.Error())
	}

	// Set port.
	port := flag.Int("port", 8081, "port for serving web application")
	flag.Parse()

	// Set route handler.
	mux := defaultMux()

	urlHandler := handleHTTPRequest(mux)

	hostname := fmt.Sprintf(":%d", *port)

	fmt.Println("Serving at " + hostname)
	http.ListenAndServe(hostname, urlHandler)
}

func apiResponseTemplate(w http.ResponseWriter, responseStatus string, mesg string, httpStatus int) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(httpStatus)
	fmt.Fprintln(w, `{"status":`+responseStatus+`,"mesg":"`+mesg+`"}`)
}
