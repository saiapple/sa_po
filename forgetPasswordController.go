package main

import (
	"bytes"
	"database/sql"
	"fmt"
	"html/template"
	"math/rand"
	"net/http"
	"net/smtp"
	"os"
	"strconv"

	"golang.org/x/crypto/bcrypt"
)

func loadForgetPasswordPage(w http.ResponseWriter, r *http.Request) {
	isLoggined(w, r)

	w.Header().Set("Content-Type", "text/html")

	t, err := template.ParseFiles("./templates/forget.html")

	if err != nil {
		panic(err)
	}

	err = t.Execute(w, nil)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func loadResetPasswordPage(w http.ResponseWriter, r *http.Request) {
	isLoggined(w, r)

	w.Header().Set("Content-Type", "text/html")

	resetKey := r.URL.Query()["resetKey"][0]

	if resetKey == "" {
		http.Redirect(w, r, "/login", http.StatusFound)
		return
	}

	var checkKey string

	err := db.QueryRow("SELECT reset_key FROM user_reset_key WHERE reset_key=? AND is_valid=1", resetKey).Scan(
		&checkKey,
	)

	if err == sql.ErrNoRows {
		fmt.Fprint(w, "Invalid reset key")
		return
	}

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	t, err := template.ParseFiles("./templates/reset.html")

	if err != nil {
		panic(err)
	}

	err = t.Execute(w, nil)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func sendResetLink(w http.ResponseWriter, r *http.Request) {
	user := User{}

	resetEmail := r.FormValue("email")

	err := db.QueryRow("SELECT id, email, COALESCE(gmail, '') as gmail FROM user WHERE email=?", resetEmail).Scan(
		&user.id,
		&user.email,
		&user.gmail,
	)

	if err != nil {
		if err == sql.ErrNoRows {
			apiResponseTemplate(w, "false", "Invalid email!", http.StatusUnprocessableEntity)
			return
		}

		apiResponseTemplate(w, "false", "Failed validate user email!", http.StatusInternalServerError)
		return
	}

	if user.gmail.String != "" {
		apiResponseTemplate(w, "false", "This email didn't support reset password. Login with google.", http.StatusInternalServerError)
		return
	}

	gmailAuth := smtp.PlainAuth("", os.Getenv("MAIL_USERNAME"), os.Getenv("MAIL_PASSWORD"), os.Getenv("MAIL_HOST"))

	t, _ := template.ParseFiles("./templates/reset-password-email.html")

	var body bytes.Buffer

	headers := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
	_, err = body.Write([]byte(fmt.Sprintf("To:"+user.email+"\nSubject: Password reset\n%s\n", headers)))

	if err != nil {
		fmt.Println(err.Error())
		apiResponseTemplate(w, "false", "Failed to send reset email!", http.StatusInternalServerError)
		return
	}

	_, resetKey, _ := generateUniqueResetKey(user)

	data := struct {
		ResetLink string
	}{
		"http://" + r.Host + "/reset-password?resetKey=" + resetKey,
	}

	err = t.Execute(&body, data)

	if err != nil {
		fmt.Println(err.Error())
		apiResponseTemplate(w, "false", "Failed to send reset email!", http.StatusInternalServerError)
		return
	}

	err = smtp.SendMail(
		os.Getenv("MAIL_HOST")+":"+os.Getenv("MAIL_PORT"),
		gmailAuth,
		os.Getenv("MAIL_USERNAME"),
		[]string{user.email},
		body.Bytes(),
	)

	if err != nil {
		fmt.Println(err.Error())
		apiResponseTemplate(w, "false", "Failed to send reset email!", http.StatusInternalServerError)
		return
	}

	apiResponseTemplate(w, "true", "Reset email send!", http.StatusOK)
}

func updatePassword(w http.ResponseWriter, r *http.Request) {
	user := User{}

	resetPassword := r.FormValue("password")
	resetKey := r.FormValue("resetKey")

	var checkKey string

	err := db.QueryRow("SELECT u.id, urk.reset_key FROM user_reset_key urk LEFT JOIN user u ON urk.user_id=u.id WHERE urk.reset_key=? AND urk.is_valid=1", resetKey).Scan(
		&user.id,
		&checkKey,
	)

	if err == sql.ErrNoRows {
		apiResponseTemplate(w, "false", "Invalid reset key!", http.StatusUnprocessableEntity)
		return
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(resetPassword), bcrypt.DefaultCost)

	if err != nil {
		apiResponseTemplate(w, "false", "Failed to update password!", http.StatusInternalServerError)
		return
	}

	_, err = db.Exec("UPDATE user SET password=? WHERE id=?", hashedPassword, user.id)

	if err != nil {
		apiResponseTemplate(w, "false", "Failed to update password!", http.StatusInternalServerError)
		return
	}

	db.Exec("UPDATE user_reset_key SET is_valid=0 WHERE reset_key=?", resetKey)

	apiResponseTemplate(w, "true", "User password updated.", http.StatusCreated)
}

func generateUniqueResetKey(user User) (bool, string, error) {
	isUnique := false

	validKey := ""

	err = db.QueryRow("SELECT reset_key FROM user_reset_key WHERE user_id=? AND is_valid=1", user.id).Scan(&validKey)

	switch {
	case err == sql.ErrNoRows:
		for !isUnique {
			randInt := strconv.Itoa(rand.Intn(100))
			hashedKey := strconv.Itoa(user.id) + randInt
			resetKey, err := bcrypt.GenerateFromPassword([]byte(hashedKey), bcrypt.DefaultCost)

			if err != nil {
				return false, "", err
			}

			var checkKey string

			err = db.QueryRow("SELECT reset_key FROM user_reset_key WHERE reset_key=?", resetKey).Scan(&checkKey)

			if err != nil && err != sql.ErrNoRows {
				return false, "", err
			}

			if checkKey == string(resetKey) {
				continue
			}

			_, err = db.Exec("INSERT INTO user_reset_key(user_id, reset_key) VALUES (?, ?)", user.id, resetKey)

			if err != nil {
				return false, "", err
			}

			isUnique = true
			validKey = string(resetKey)
		}
	}

	return true, validKey, nil
}
