package main

import (
	"database/sql"
	"html/template"
	"net/http"

	"golang.org/x/crypto/bcrypt"
)

func loadRegisterPage(w http.ResponseWriter, r *http.Request) {
	isLoggined(w, r)

	w.Header().Set("Content-Type", "text/html")

	t, err := template.ParseFiles("./templates/register.html")

	if err != nil {
		panic(err)
	}

	err = t.Execute(w, nil)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func processRegister(w http.ResponseWriter, r *http.Request) {
	email := r.FormValue("email")
	password := r.FormValue("password")

	var userCheck string

	err := db.QueryRow("SELECT email FROM user WHERE email=?", email).Scan(&userCheck)

	switch {
	case err == sql.ErrNoRows:
		hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)

		if err != nil {
			apiResponseTemplate(w, "false", "Failed to create user account!", http.StatusInternalServerError)
			return
		}

		_, err = db.Exec("INSERT INTO user(email, password) VALUES (?, ?)", email, hashedPassword)

		if err != nil {
			apiResponseTemplate(w, "false", "Failed to create user account!", http.StatusInternalServerError)
			return
		}

		user := User{}

		db.QueryRow("SELECT id FROM user WHERE email=?", email).Scan(
			&user.id,
		)

		_, sessionKey, _ := generateUniqueSessionKey(user)

		setSession(w, r, "userSession", sessionKey)

		apiResponseTemplate(w, "true", "User account created.", http.StatusCreated)

	case userCheck == email:
		apiResponseTemplate(w, "false", "Google account is already registered!", http.StatusUnprocessableEntity)

	case err != nil:
	default:
		apiResponseTemplate(w, "false", "Failed to create user account!", http.StatusInternalServerError)
		return
	}
}

func processRegisterGoogle(w http.ResponseWriter, r *http.Request) {
	email := r.FormValue("email")
	gmail := r.FormValue("gmail")
	name := r.FormValue("name")

	var userCheck string

	err := db.QueryRow("SELECT COALESCE(gmail, '') FROM user WHERE gmail=?", gmail).Scan(&userCheck)

	switch {
	case err == sql.ErrNoRows:
		_, err = db.Exec("INSERT INTO user(name, email, gmail) VALUES (?, ?, ?)", name, email, gmail)

		if err != nil {
			apiResponseTemplate(w, "false", "Failed to create user account!", http.StatusInternalServerError)
			return
		}

		user := User{}

		db.QueryRow("SELECT id FROM user WHERE email=?", email).Scan(
			&user.id,
		)

		_, sessionKey, _ := generateUniqueSessionKey(user)

		setSession(w, r, "userSession", sessionKey)

		apiResponseTemplate(w, "true", "User account created.", http.StatusCreated)

	case userCheck == gmail:
		apiResponseTemplate(w, "false", "Email is already taken!", http.StatusUnprocessableEntity)

	case err != nil:
	default:
		apiResponseTemplate(w, "false", "Failed to create user account!", http.StatusInternalServerError)
		return
	}
}
