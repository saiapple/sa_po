package main

import (
	"html/template"
	"net/http"
)

func defaultMux() *http.ServeMux {
	mux := http.NewServeMux()
	mux.HandleFunc("/", defaultHandler)
	mux.Handle("/resources/", http.StripPrefix("/resources/", http.FileServer(http.Dir("resources"))))

	return mux
}

func defaultHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")

	t, err := template.ParseFiles("./templates/notfound.html")

	if err != nil {
		panic(err)
	}

	err = t.Execute(w, nil)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func handleHTTPRequest(fallback http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		path := r.URL.Path

		if r.Method == http.MethodGet {
			handleGETMethod(&path, w, r, fallback)
		} else if r.Method == http.MethodPost {
			handlePOSTMethod(&path, w, r, fallback)
		} else {

		}

		return
	}
}

func handleGETMethod(path *string, w http.ResponseWriter, r *http.Request, fallback http.Handler) {
	switch *path {
	case "/":
		serveIndexPage(w, r)
	case "/register":
		loadRegisterPage(w, r)
	case "/login":
		loadLoginPage(w, r)
	case "/profile":
		loadUserProfilePage(w, r)
	case "/complete-register":
		profileCompletePage(w, r)
	case "/forget-password":
		loadForgetPasswordPage(w, r)
	case "/reset-password":
		loadResetPasswordPage(w, r)
	default:
		fallback.ServeHTTP(w, r)
	}
}

func handlePOSTMethod(path *string, w http.ResponseWriter, r *http.Request, fallback http.Handler) {
	switch *path {
	case "/rest/register":
		processRegister(w, r)
	case "/rest/register-google":
		processRegisterGoogle(w, r)
	case "/rest/login":
		processLogin(w, r)
	case "/rest/login-google":
		processLoginGoogle(w, r)
	case "/rest/complete-register":
		completeUserProfile(w, r)
	case "/rest/update-profile":
		updateUserProfile(w, r)
	case "/rest/send-reset-link":
		sendResetLink(w, r)
	case "/rest/reset-password":
		updatePassword(w, r)
	case "/logout":
		logoutSession(w, r)
	default:
		fallback.ServeHTTP(w, r)
	}
}

func serveIndexPage(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")

	t, err := template.ParseFiles("./templates/index.html")

	if err != nil {
		panic(err)
	}

	err = t.Execute(w, nil)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
