package main

import (
	"database/sql"
	"html/template"
	"net/http"
)

func loadUserProfilePage(w http.ResponseWriter, r *http.Request) {
	isGuest(w, r)

	w.Header().Set("Content-Type", "text/html")

	t, err := template.ParseFiles("./templates/profile.html")

	if err != nil {
		panic(err)
	}

	user, err := LoadUserProfile(w, r)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	if user.profileComplete == 0 {
		http.Redirect(w, r, "/complete-register", http.StatusFound)
		return
	}

	data := struct {
		ID              int
		Name            string
		Address         string
		Email           string
		Phone           string
		Gmail           string
		ProfileComplete int
	}{
		user.id,
		user.name.String,
		user.address.String,
		user.email,
		user.phone.String,
		user.gmail.String,
		user.profileComplete,
	}

	err = t.Execute(w, data)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// ProfileCompletePage page to show name, password, address form for user whose profile is not yet complete.
func profileCompletePage(w http.ResponseWriter, r *http.Request) {
	isGuest(w, r)

	w.Header().Set("Content-Type", "text/html")

	t, err := template.ParseFiles("./templates/complete-profile.html")

	if err != nil {
		panic(err)
	}

	user, err := LoadUserProfile(w, r)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	if user.profileComplete == 1 {
		http.Redirect(w, r, "/profile", http.StatusFound)
		return
	}

	data := struct {
		ID              int
		Name            string
		Address         string
		Email           string
		Phone           string
		Gmail           string
		ProfileComplete int
	}{
		user.id,
		user.name.String,
		user.address.String,
		user.email,
		user.phone.String,
		user.gmail.String,
		user.profileComplete,
	}

	err = t.Execute(w, data)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// CompleteUserProfile function to insert name, password, address for user whose profile is not yet complete.
func completeUserProfile(w http.ResponseWriter, r *http.Request) {
	name := r.FormValue("name")
	address := r.FormValue("address")
	email := r.FormValue("email")
	phone := r.FormValue("phone")

	user, err := LoadUserProfile(w, r)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	_, err = db.Exec("UPDATE user SET name=?, address=?, email=?, phone=?, profile_complete=1 where id=?", name, address, email, phone, user.id)

	if err != nil {
		apiResponseTemplate(w, "false", "Failed to complete user profile!", http.StatusInternalServerError)
		return
	}

	apiResponseTemplate(w, "true", "User profile complete!", http.StatusOK)
}

// UpdateUserProfile function to update user profile.
func updateUserProfile(w http.ResponseWriter, r *http.Request) {
	name := r.FormValue("name")
	address := r.FormValue("address")
	email := r.FormValue("email")
	phone := r.FormValue("phone")

	user, err := LoadUserProfile(w, r)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	var emailCheck string

	err = db.QueryRow("SELECT email FROM user WHERE email=? AND id!=?", email, user.id).Scan(&emailCheck)

	switch {
	case err == sql.ErrNoRows:
		_, err = db.Exec("UPDATE user SET name=?, address=?, email=?, phone=? where id=?", name, address, email, phone, user.id)

		if err != nil {
			apiResponseTemplate(w, "false", "Failed to update user profile!", http.StatusInternalServerError)
			return
		}

	case emailCheck == email:
		apiResponseTemplate(w, "false", "Email is already taken!", http.StatusUnprocessableEntity)
		return
	}

	apiResponseTemplate(w, "true", "User profile has been updated!", http.StatusOK)
}

// LogoutSession .
func logoutSession(w http.ResponseWriter, r *http.Request) {
	deleteSession(w, r, "userSession")
	apiResponseTemplate(w, "true", "Success", http.StatusOK)
}

// LoadUserProfile function to load user profile.
func LoadUserProfile(w http.ResponseWriter, r *http.Request) (User, error) {
	user := User{}

	err := db.QueryRow("SELECT u.id, COALESCE(u.name, '') as name, COALESCE(u.address, '') as address, u.email, COALESCE(u.phone, '') as phone, COALESCE(u.gmail, ''), u.profile_complete as gmail FROM user_session us LEFT JOIN user u ON us.user_id=u.id WHERE us.session_token=?", getSession(w, r, "userSession")).Scan(
		&user.id,
		&user.name,
		&user.address,
		&user.email,
		&user.phone,
		&user.gmail,
		&user.profileComplete,
	)

	if err != nil {
		return user, err
	}

	return user, nil
}
