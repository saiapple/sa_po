package main

import "net/http"

// isLoggined to check whether user is login or not
func isLoggined(w http.ResponseWriter, r *http.Request) {
	isLogin := getSession(w, r, "userSession")

	if isLogin != "" {
		http.Redirect(w, r, "/profile", http.StatusFound)
		return
	}
}

// isGuest to check whether user is guest or not
func isGuest(w http.ResponseWriter, r *http.Request) {
	isLogin := getSession(w, r, "userSession")

	if isLogin == "" {
		http.Redirect(w, r, "/login", http.StatusFound)
		return
	}
}
