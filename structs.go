package main

import "database/sql"

// User .
type User struct {
	id              int
	name            sql.NullString
	address         sql.NullString
	email           string
	phone           sql.NullString
	gmail           sql.NullString
	profileComplete int
	password        sql.NullString
}
