package main

import (
	"net/http"
)

// getSession .
func getSession(w http.ResponseWriter, r *http.Request, sessionKey string) string {
	session, err := store.Get(r, "authTestCookie")

	if err != nil {
		panic(err.Error())
	}

	if session.Values[sessionKey] == nil {
		return ""
	}

	return session.Values[sessionKey].(string)
}

// setSession .
func setSession(w http.ResponseWriter, r *http.Request, sessionKey string, value string) {
	session, err := store.Get(r, "authTestCookie")

	if err != nil {
		panic(err.Error())
	}

	session.Values[sessionKey] = value
	session.Save(r, w)
}

// deleteSession .
func deleteSession(w http.ResponseWriter, r *http.Request, sessionKey string) {
	session, err := store.Get(r, "authTestCookie")

	if err != nil {
		panic(err.Error())
	}

	session.Values[sessionKey] = ""
	session.Save(r, w)
}
